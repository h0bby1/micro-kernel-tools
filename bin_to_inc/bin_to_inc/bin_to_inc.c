#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#if !defined(MAXPATHLEN)
#define MAXPATHLEN  256
#endif

char *dirname(const char *path)
{
    static char *bname;
    const char *endp;

    if (bname == NULL) {
        bname = (char *) malloc(MAXPATHLEN);
        if (bname == NULL)
            return (NULL);
    }

    /* Empty or NULL string gets treated as "." */
    if (path == NULL || *path == '\0') {
        strncpy(bname, ".", MAXPATHLEN);
        return (bname);
    }

    /* Strip trailing slashes */
    endp = path + strlen(path) - 1;
    while (endp > path && *endp == '/')
        endp--;

    /* Find the start of the dir */
    while (endp > path && *endp != '/')
        endp--;

    /* Either the dir is "/" or there are no slashes */
    if (endp == path) {
        strncpy(bname, *endp == '/' ? "/" : ".", MAXPATHLEN);
        return (bname);
    }

    do {
        endp--;
    } while (endp > path && *endp == '/');

    if (endp - path + 2 > MAXPATHLEN) {
        return (NULL);
    }
    strcpy(bname, path);
    bname[(endp - path) + 1] = 0;

    return (bname);
}

char *basename (char* path)
{
	char *ptr = strrchr (path, '/');
	return ptr ? ptr + 1 : (char*)path;
}
void rep_string(char *string)
{
	unsigned int		n;
	n=strlen(string);
	while(n--){ if(string[n]==0)return; if(string[n]=='\\')string[n]='/'; }
}

int main(int argc,char **argv)
{
	FILE		*in;
	FILE		*out;
	char		 file_buffer[4096];
	char		 file_path[1024];
	char		 file_name[1024];
	char		 format[32];
	char		 module_name[1024];
	char		 target_path[1024];
	char		 target_file[1024];
	unsigned int *file_data=NULL;
	unsigned int file_size;
	unsigned int n,i;
	unsigned int cnt;

	if(argc>1)
	{
		strcpy(file_path,argv[1]);
	}
	else
	{
		printf("no file specified ! \n");
		goto end;
	}
	rep_string	(file_path);



	
	strcpy		(file_name		,basename(file_path));
	strcpy		(module_name	,file_name);
	n	=strlen	(module_name);
	if(n>4)
	{
		i	=n-4;
		while(i<n){	if(module_name[i]=='.'){module_name[i]=0;break;} n++;}
	}
	if(argc>2)
	{
		strcpy		(target_path,argv[2]);
		rep_string	(target_path);
	}
	else
	{
		strcpy		(target_path,dirname(file_path));
	}


	if(argc>3)
	{
		strcpy(format,argv[3]);
	}
	else
	{
		strcpy(format,"ASM");
	}



	in	=	fopen(file_path,"rb");
	fseek	(in,0,SEEK_END);
	file_size=ftell(in);
	rewind(in);
	file_data	=	malloc(file_size+4);
	fread		(file_data,file_size,1,in);
	fclose		(in);
	

	
	strcpy	(target_file	,target_path);
	strcat	(target_file	,"/");
	strcat	(target_file	,module_name);

	if(!stricmp(format,"ASM"))
		strcat	(target_file	,".inc");
	else
		strcat	(target_file	,".h");


	out=fopen(target_file,"wb");
	if(!out)goto end;

	if(!stricmp(format,"ASM"))
	{
		fprintf(out,"align 16\n",module_name);
		fprintf(out,"binary_data_%s : \ndd ",module_name);
	}
	else if(!stricmp(format,"C"))
	{
		fprintf(out,"unsigned int binary_data_%s [] = { \n ",module_name);
	}
	

	n=0;
	cnt=0;
	while(n*4<file_size)
	{
		if(((n+1)*4)>=file_size)
			fprintf(out,"0x%8.8x\n",file_data[n]);
		else
			fprintf(out,"0x%8.8x," ,file_data[n]);		

		cnt++;
		n++;

		if((cnt >= 16)&&(n*4<file_size))
		{
			fprintf	(out,"\n");

			if(!stricmp(format,"ASM"))fprintf(out,"dd ");
	
			cnt		=0;
		}
	}

	if(!stricmp(format,"ASM"))
	{
		fprintf(out,"binary_data_%s_end : \n\n",module_name);
	}
	else if(!stricmp(format,"C"))
	{
		fprintf(out,"};\n\n");
	}
	
	fclose(out);

		
	end:

	if(file_data)free(file_data);
	return 0;

}