///
 // vcprojtomake.cpp : Defines the entry point for the console application.
//

#include "StdAfx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define TARGET_PLAFORME_LINUX

#ifdef TARGET_PLAFORME_LINUX
# define MAKEFILE_SUFFIX ".linux"
# include <limits.h>
# if ( __WORDSIZE == 64 )
#  define GCC32PARAM   "-m32 "
# else // __WORDSIZE == 64
#  define GCC32PARAM ""
# endif
#endif // TARGET_PLAFORME_LINUX

#ifdef TARGET_PLAFORME_MACOS
# define MAKEFILE_SUFFIX ".mac"
# define GCC32PARAM ""
#endif




typedef struct
{
	char			str[256];
}post_arg_t;
typedef struct
{
	char			file_cmd[256];
	post_arg_t		args[32];
	int				n_args;
}post_cmd_t;

typedef struct
{
	char		source_project[512];
	char		name[1024];
	char		output_dir[1024];
	char		prefix[1024];
	char		includes[1024];
	char		preproc[1024];
	post_cmd_t	post_cmd[32];
	unsigned int n_post_cmds;
	char		target[1024*8];
	char		target_incs[1024*8];
	char		target_def[1024*8];
	char		target_dep[1024*8];
	char		incs_var[128];
	char		def_var[128];
	char		obj_var[128];
	char		dep_var[128];
	
}global_config;
	
typedef struct
{
	char	name[512];
	char	short_name[512];
	char	includes[512];
	char	preproc[512];
}file_conf;

typedef struct
{
char		 source_proj[512];
char		 path[512];
char		 abs_path[512];
char		 make_path[512];
char		 short_path[512];
file_conf	 confs[10];
unsigned int num_conf;
unsigned int ext_id;
}target_file;


class project_t
{
friend class solution_t;
public:
					project_t		();

	global_config	*get_gconf		(char *name,const char *p);
	void			ReadVcFile		(char *source_file,const char *base_files_path,const char *solution_path);
	void			WriteMakeFile	(char *sol_dir,char *file,const char *conf,const char *make_dir,char *out_binary,unsigned int type);


	void			dump			(FILE *s,int x);

private:

	char			ID[255];
	char			file_path[255];
	char			make_file_path[255];
	char			name[255];

	char			depend_list_id[20][255];
	char			depend_list_name[20][255];
	char			depend_list_mf[20][255];
	int				num_depends;

	target_file		files[255];
	file_conf		*cur_conf;
	global_config	*cur_gconf;

	global_config	gconfigs[100];
	unsigned int	num_gbl_conf;

	unsigned int	num_files;
	target_file		*current_file;
};

class solution_t
{
public:
void			ReadSln			(char *file,char *MakeFilePath);
void			WriteMakeFile	(unsigned int proj_id,char *makefile_path,char *configuration);
project_t		*GetProjectById(char *id);
char			basedir[255];
private:
project_t		projects[30];
unsigned int	num_project;

};

char msg[1024];
#ifndef _WIN32
#define	stricmp strcasecmp
#endif

int get_ext_id(char *file)
{
	char *last_dot=NULL;
	int  cur_char;
	int  len_file=strlen(file);

	while( (*file)!=0)
	{
		if((*file++)=='.')last_dot=file;
	}

	if(last_dot!=NULL)
	{
		if(!strcmp(last_dot,"c"))
		{
			return 1;
		}
		if(!strcmp(last_dot,"cpp"))
		{
			return 2;
		}
		if(!strcmp(last_dot,"asm"))
		{
			return 3;
		}
		return 0;
	}

	return -1;
}
int extract_quote(char *quoted,int len,char *out)
{
int read_q=0;
while(quoted[read_q]!='"')
{
	read_q++;
	if(read_q>=len)break; 
}
read_q++;
while(quoted[read_q]!='"')
{
	(*(out++))=quoted[read_q];
	read_q++;
	if(read_q>=len)break;
}

(*out)=0;
return read_q;

}
void rep_bsl(char *p)
{
	while( (*p)!=0)
	{
		if(*p=='\\')*p='/';
		
		p++;
	}
}

char *basename(char *src)
{
	char *ret;

	ret = strrchr(src, '/');
	return (ret ? ret + 1 : src);
}


int dirname(char *src)
{
	int ret;
	int i=0;

	while(*src!=0)
	{
		if( ((*src)=='/')|| ((*src)=='\\'))ret=i;
		src++;
		i++;
	}

	return ret;
}

void remove_last_path(char *path,int len_path)
{
	int len=strlen(path);
	if( (path[len]=='/')||
		(path[len]=='\\'))
	{
		len--;
	}
	
	while(len--)
	{
		if( (path[len]=='/')|
			(path[len]=='\\'))
		{
			memset(&path[len],0,len_path-len-1);
			return;
		}
	}

	memset(path,0,len_path);
}

void remove_first_path(char *path)
{
	char  temp_path[256];
	char  pref[32];

	char  *p;
	char  *temp=temp_path;
	char  *src=path;

	memset(pref,0,32);
	memset (temp_path,0,256);
	p   =pref;
	src =path;
	
	while(((*src)=='/')||((*src)=='\\')){*(temp++)=*(src++);}
	while(((*src)!='/')&&((*src)!='\\')){*src++;if((*src)==0)return;}
	while(((*src)=='/')||((*src)=='\\')){*src++;}
	while((*src)!=0)					{*(temp++)=(*src++);}

	strcpy(path,temp_path);
}
void get_last_path(char *fp,const char *dir)
{
	int last_slash	=	0;
	int lendir		= strlen(dir);

	while((dir[lendir]=='/')||(dir[lendir]=='\\'))				{lendir--;}
	while((lendir>0)&&(dir[lendir]!='/')&&(dir[lendir]!='\\'))	{lendir--;}

	if((dir[lendir]=='/')||(dir[lendir]=='\\'))lendir++;

	while(  (dir[lendir]!=0)&&(dir[lendir]!='/')&&(dir[lendir]!='\\'))
	{
		*(fp++)=dir[lendir];
		lendir++;
	}
	*fp=0;
}

void get_first_path(char *fp,const char *dir)
{
	while(((*dir)=='/')||((*dir)=='\\')){*(fp++)=*(dir++);}

	while(  ((*dir)!='/')&&
			((*dir)!='\\')&&
			((*dir)!=0) )
	{
		*(fp++)=*(dir++);
	}
	*fp=0;
}


bool is_path_inside(const char *test_path,const char *base_path)
{
	char last[255];
	char first[255];
	
	get_first_path(first,test_path);
	get_last_path (last,base_path);
	
	if(!stricmp(first,last))
		return 1;
	else
		return 0;


}


void get_real_path(char *out,const char *basedir,const char *rel)
{
	char temp[256];
	char temp2[256];

	strcpy(temp ,basedir);
	strcpy(temp2,rel);

	
	while(1)
	{
		char fp[256];

	
		get_first_path(fp,temp2);

		if(!strcmp(fp,".."))
		{
			remove_last_path  (temp ,256);
			remove_first_path (temp2);
		}
		else
		{
			break;
		}
	}
	
	if(strlen(temp))
	{
		strcpy(out,temp);
		strcat(out,"/");
		strcat(out,temp2);
	}
	else
	{
		strcpy(out,temp2);
	}
}


const char  *trim_left	(const char *dir,const char *basedir)
{
	const char *os=dir;
	int  l=strlen(basedir);
	while( ((*dir++)==(*basedir++)) && (*basedir!=0) && (*dir!=0) )
	{
		l--;
	}

	if(l>1)return os;

	while( ((*dir)=='/')|| ((*dir)=='\\')){ dir++; }

	return dir;

}

char *path_list[128]={ "D:\\mingw\\gtk-2.10.6\\","D:\\mingw\\cairo-1.2.6\\","D:\\mingw\\glib-dev-2.12.4\\","D:\\mingw\\pango-dev-1.14.8\\","D:\\mingw\\atk-dev-1.12.2\\","D:\\cygwin\\usr\\","D:\\mingw\\gtk-2.10.6\\lib\\glib-2.0\\",""};

void replace_abs_include(char *dest,const char *include_path,char **path_list)
{
	int  i=0;

	while( path_list[i][0]!=0 )
	{
		int l=strlen(path_list[i]);

		if(!strncmp(include_path,path_list[i],l))
		{
			strcat(dest,"/usr/");
			strcat(dest,trim_left(include_path,path_list[i]));
			return;
		}
		i++;
	}

	strcat(dest,include_path);
}


void reset_file_conf	(file_conf *conf)
{
	memset(conf->name,0,512);
	memset(conf->short_name,0,512);
	memset(conf->includes,0,512);
	memset(conf->preproc,0,512);
}

void reset_target_file	(target_file *file)
{
	memset(file->source_proj,0,512);
	memset(file->path,0,512);
	memset(file->abs_path,0,512);
	memset(file->make_path,0,512);
	memset(file->short_path,0,512);
	for(int i=0;i<10;i++)
	{
		reset_file_conf(&file->confs[i]);
	}
	file->num_conf=0;
	file->ext_id=0;
}
void reset_gconf		(global_config *conf)
{
	memset(conf->source_project,0,512);
	memset(conf->name,0,1024);
	memset(conf->prefix,0,1024);
	memset(conf->includes,0,1024);
	memset(conf->preproc,0,1024);
	memset(conf->target,0,1024*4);
	memset(conf->target_incs,0,1024*4);
	memset(conf->target_def,0,1024*4);
	memset(conf->target_dep,0,1024*4);
	memset(conf->incs_var,0,128);
	memset(conf->def_var,0,128);
	memset(conf->obj_var,0,128);
	memset(conf->dep_var,0,128);
}

global_config *project_t::get_gconf(char *name,const char *project)
{
	for(int i=0;i<num_gbl_conf;i++)
	{
		if( (!strcmp(gconfigs[i].name,name))&&(!strcmp(gconfigs[i].source_project,project)) ) return &gconfigs[i];
	}
	return NULL;

}

project_t::project_t()
{
	num_depends=0;
	num_files=0;
	num_gbl_conf=0;
	current_file=NULL;
	cur_gconf=NULL;
	cur_conf=NULL;
	


	memset			(ID,0,255);
	memset			(file_path,0,255);
	memset			(make_file_path,0,255);
	memset			(name,0,255);
	
	for(int i=0;i<20;i++)
	{
		memset(depend_list_id[i],0,255);
		memset(depend_list_name[i],0,255);
		memset(depend_list_mf[i],0,255);
	}
	for(int i=0;i<100;i++)
	{
		reset_gconf(&gconfigs[i]);
	}

	num_depends=0;
	for(int i=0;i<255;i++)
	{
		reset_target_file	(&files[i]);
	}
	
	
	cur_conf	=NULL;
	cur_gconf	=NULL;

	num_gbl_conf=0;
	num_files	=0;
	current_file=NULL;
}

void project_t::dump			(FILE *s,int x)
{
	int i;
	printf("[%d] Project : %s \r\n"	,x,name);
	printf("%s \r\n"			, ID);
	printf("\t(%s) \r\n"		, file_path);
	
	printf("\tConfigurations : \r\n");
	for(i=0;i<num_gbl_conf;i++)
	{
		printf("\t\t%s\r\n",gconfigs[i].name);
	}
	printf("\tDependences : \r\n");
	for(i=0;i<num_depends;i++)
	{
		printf("\t\t%s\r\n",depend_list_id[i]);
	}
	printf("\r\n");

}

void project_t::ReadVcFile(char *source_file,const char *base_files_path,const char *solution_path)
{
	FILE			*s;
	char			*file_buffer;
	char			*p;
	int				len;
	unsigned int	num_marker;
	char			markup[1024];
	char			inc_path[1024];
	char			post_cmd[1024];
	char			prepro[1024];
	char			name[1024];
	char			file_name[1024];
	char			proj_base_path[1024];
	int				dirlen=dirname(source_file);

	strncpy(proj_base_path,source_file,dirlen);
	proj_base_path[dirlen]=0;

	current_file=NULL;
	cur_gconf=NULL;
	cur_conf=NULL;
	

	s=fopen(source_file,"rb");
	if(!s)
	{
		printf("\tCannot Open Project File : '");
		printf(source_file);
		printf("' skipping ... \r\n");
		return;
	}

	fseek(s,0,SEEK_END);
	len=ftell(s);
	file_buffer=(char *)calloc(len,1);
	rewind	(s);
	fread	(file_buffer,len,1,s);
	fclose	(s);


	p=strtok(file_buffer,"<>");
	num_marker=0;

	while(p!=NULL)
	{
		strcpy(markup,p);
		num_marker++;

		if(num_marker>=107)
		{
			int oo;
			oo=num_marker;
		}

		if(!strncmp(markup,"Configuration\r\n",15))
		{		
			char *tt;
			char *d;

			cur_gconf=&gconfigs[num_gbl_conf];

			cur_gconf->n_post_cmds							=0;
			cur_gconf->post_cmd[0].n_args		=0;
			
			memset(cur_gconf->post_cmd[0].file_cmd,0,sizeof(cur_gconf->post_cmd[0].file_cmd));
			
			tt=strstr(markup,"Name=")+strlen("Name=");		

			while(*tt=='"'){tt++;}
			d=cur_gconf->name;
			while(*tt!='"'){*(d++)=*(tt++);}
			*d=0;

			tt=cur_gconf->name;
			d=cur_gconf->prefix;

			while(*tt!=0)
			{
				if(*(tt)=='|')
				{
					*(d++)='_';
					tt++;
				}
				else if(*(tt)==' ')
				{
					*(d++)='_';
					tt++;
				}

				else
					*(d++)=/*tolower*/(*(tt++));
			}
			*d=0;

			//printf("\t%s\r\n",cur_gconf->prefix);
			strcpy(cur_gconf->source_project,source_file);

			memset(cur_gconf->output_dir,0,64);

			tt=strstr(markup,"OutputDirectory=")+strlen("OutputDirectory=");		
			if(tt)
			{
				char o_dir[1024];
				int n,l;

				while(*tt=='"'){tt++;}
				d=o_dir;
				while(*tt!='"'){*(d++)=*(tt++);}
				*d=0;

				l	=	strlen(o_dir);
				n	=	0;
				while(n<l)
				{
					if(((n+1)<l)&((o_dir[n]=='$')&&(o_dir[n+1]=='(')))
					{
						char var_name[64];
						int  vnlen;

						n	+=2;
						vnlen=0;
						while(n<l)
						{
							if(o_dir[n]==')'){n++;break;}
							var_name[vnlen]=o_dir[n];
							vnlen++;
							n++;
						}
						var_name[vnlen]=0;
						if(!strcmp(var_name,"SolutionDir"))
						{
							strcat(cur_gconf->output_dir,solution_path);
							strcat(cur_gconf->output_dir,"/");
						}
						if(!strcmp(var_name,"ConfigurationName"))
						{
							strcat(cur_gconf->output_dir,cur_gconf->name);
						}

						
					}
					
					int clen=strlen(cur_gconf->output_dir);
					cur_gconf->output_dir[clen]		=o_dir[n];
					cur_gconf->output_dir[clen+1]	=0;
					
					n++;
				}
			}
		}	
		else if(!strncmp(markup,"/Configuration",14))
		{	
			if(cur_gconf!=NULL)
			{
				num_gbl_conf++;
			}

			cur_gconf=NULL;
		}
		else if(!strncmp(markup,"File\r\n",6))
		{
			int ext_id;

			char *tt;
			char *d;
			tt=strstr(markup,"RelativePath=")+strlen("RelativePath=");

			while(*tt=='"'){tt++;}

			d=file_name;
			while(*tt!='"'){*(d++)=*(tt++);}
			*d=0;
			
			ext_id=get_ext_id(file_name);
			if(ext_id>0)
			{
				current_file=&files[num_files];
				current_file->num_conf=0;
				current_file->ext_id=ext_id;
				
				strcpy  (current_file->source_proj,source_file);
				strcpy	(current_file->path,file_name);
				rep_bsl (current_file->path);
				
				get_real_path(current_file->abs_path,proj_base_path,current_file->path);
				rep_bsl (current_file->abs_path);
			

				if(is_path_inside(current_file->abs_path,base_files_path))
				{
					strcpy(current_file->make_path,"./");
					strcat(current_file->make_path,trim_left(current_file->abs_path,base_files_path));
				}
				else
				{
					strcpy(current_file->make_path,current_file->abs_path);
				}
				

	
				tt	=basename(current_file->path);
				d	=current_file->short_path;
				while((*tt!=0)&&(*tt!='.'))
				{
					*(d++)=*(tt++);
				}
				*d=0;
			}
		}
		else	if(!strncmp(markup,"FileConfiguration\r\n",19))
		{

			if(strstr(markup,"ExcludedFromBuild=\"true\"")==NULL)
			{
				if(current_file!=NULL)
				{
					cur_conf=&current_file->confs[current_file->num_conf];



					if(strstr(markup,"Name=")!=NULL)
					{

						char *tt;
						char *d;
						tt=strstr(markup,"Name=")+strlen("Name=");

						while(*tt=='"'){tt++;}

						d=cur_conf->name;
						while(*tt!='"'){*(d++)=*(tt++);}

						*d=0;

						global_config *gc;

						gc=get_gconf(cur_conf->name,source_file);
						if(gc)
						{
							strcpy(cur_conf->preproc,"");
							strcpy(cur_conf->includes,"");

							
							strcpy(cur_conf->short_name,gc->prefix);
							strcat(cur_conf->short_name,"/");
							strcat(cur_conf->short_name,this->name);
							strcat(cur_conf->short_name,"/");
							strcat(cur_conf->short_name,current_file->short_path);
							strcat(cur_conf->short_name,".o");
						}
						else
						{
							strcpy(cur_conf->preproc,"");
							strcpy(cur_conf->includes,"");
						}

						
					}
				}
			}
			else
			{
			
			}

		}
		else	if(!strcmp(markup,"/FileConfiguration"))
		{
			if((current_file!=NULL)&&(cur_conf!=NULL))
			{
				current_file->num_conf	++;
			}
			cur_conf=NULL;
		}
		else	if(!strcmp(markup,"/File"))
		{
			int i;

			if(current_file!=NULL)
			{
				num_files++;
				current_file=NULL;
			}
		}
		else if (!strncmp(markup,"Tool\r\n",6))
		{
			char *tt;
			char *d;
			
			tt=strstr(markup,"Name=")+strlen("Name=");		

			while(*tt=='"'){tt++;}
			d=name;
			while(*tt!='"'){*(d++)=*(tt++);}

			*d=0;
			if(!strcmp(name,"VCPostBuildEventTool"))
			{
				tt=strstr(markup,"CommandLine=");
				if( (tt!=NULL)&&(strlen(tt)>0)  )
				{
					tt+=strlen("CommandLine=");

					while((*tt=='"')&&(*tt!=0)){tt++;}

					if(strncmp(tt,"\r\n",2))
					{
						d=post_cmd;
						while((*tt!='"')&&(*tt!=0))
						{
							*(d++)=*(tt++);
						}
						*d=0;

						rep_bsl(post_cmd);

						if(cur_gconf!=NULL)
						{
							int			n;
							int			start_word;
							int			args_in;
							int			len_post_cmd;
							char		*chars;
							unsigned int cmd_started;


							
			
							len_post_cmd					=	strlen(post_cmd);
							
							

							while(chars=strstr(post_cmd,"&#x0D;&#x0A;"))
							{
								unsigned int ofs,ofs_end;
								unsigned int len_tag;
								unsigned int new_pos;
								
								
								len_tag	=	strlen("&#x0D;&#x0A;");
								ofs		=	chars	-	post_cmd;
								ofs_end	=	ofs+len_tag;
								new_pos	=	ofs+1;

								post_cmd[ofs]=10;

								if(len_post_cmd>ofs_end)
									memcpy(&post_cmd[new_pos],&post_cmd[ofs_end],len_post_cmd-ofs_end);

								memset(&post_cmd[new_pos+(len_post_cmd-ofs_end)],0,len_tag-1);
							}


							n								=0;
							start_word						=n;
							args_in							=0;
							cmd_started						=0;

							while(n<(len_post_cmd+1))
							{

								if((post_cmd[n]==' ')||(post_cmd[n]==0)||(post_cmd[n]==10))
								{
									int len=n-start_word;

									if(len>0)
									{
										cmd_started=1;
										if(args_in==0)
										{
											strncpy(cur_gconf->post_cmd[cur_gconf->n_post_cmds].file_cmd,&post_cmd[start_word],len);

											cur_gconf->post_cmd[cur_gconf->n_post_cmds].file_cmd[len]=0;
											while(len--)
											{
												if(cur_gconf->post_cmd[cur_gconf->n_post_cmds].file_cmd[len]=='.')
												{
													cur_gconf->post_cmd[cur_gconf->n_post_cmds].file_cmd[len]=0;
													break;
												}
											}
										}
										else
										{
											strncpy(cur_gconf->post_cmd[cur_gconf->n_post_cmds].args[cur_gconf->post_cmd[cur_gconf->n_post_cmds].n_args].str,&post_cmd[start_word],len);
											cur_gconf->post_cmd[cur_gconf->n_post_cmds].args[cur_gconf->post_cmd[cur_gconf->n_post_cmds].n_args].str[len]=0;

											if(!strncmp(cur_gconf->post_cmd[cur_gconf->n_post_cmds].args[cur_gconf->post_cmd[cur_gconf->n_post_cmds].n_args].str,"&gt;",4))
											{
												strcpy(cur_gconf->post_cmd[cur_gconf->n_post_cmds].args[cur_gconf->post_cmd[cur_gconf->n_post_cmds].n_args].str,">");

											}

											cur_gconf->post_cmd[cur_gconf->n_post_cmds].n_args++;
										}

										args_in++;
									}
									start_word=n+1;
								}


								if((post_cmd[n]==10)||(post_cmd[n]==0))
								{
									n++;
									start_word						=n;
									args_in							=0;

									if(cmd_started==1)
										cur_gconf->n_post_cmds++;

									cmd_started=0;
								}
								

								n++;
							}
							//$(SolutionDir)/tools/mod_maker.exe $(TargetPath) $(SolutionDir)bin\image\drivers &gt; $(SolutionDir)bin\logs\$(ProjectName).log
						}
					}
				}
			}
			if(!strcmp(name,"VCCLCompilerTool"))
			{
				tt=strstr(markup,"AdditionalIncludeDirectories=");
				if( (tt!=NULL)&&(strlen(tt)>0)  )
				{
					tt+=strlen("AdditionalIncludeDirectories=");

					while((*tt=='"')&&(*tt!=0)){tt++;}

					if(strncmp(tt,"\r\n",2))
					{
						d=inc_path;
						while(*tt!='"')
						{
							*(d++)=*(tt++);
						}

						*d=0;

						
						if((current_file!=NULL)&&(cur_conf!=NULL))
						{
							global_config *gc;
							strcpy       (cur_conf->includes,inc_path);

						}
						else
						if(cur_gconf!=NULL)
						{
							strcpy(cur_gconf->includes,inc_path);
						}
					}
				}
				tt=strstr(markup,"PreprocessorDefinitions=");
				if( (tt!=NULL)&&(strlen(tt)>0)  )
				{
					tt+=strlen("PreprocessorDefinitions=");

					while(*tt=='"'){tt++;}

					if(strncmp(tt,"\r\n",2))
					{
						d=prepro;
						while(*tt!='"')
						{
							*(d++)=*(tt++);
						}

						*d=0;
						
						if((current_file!=NULL)&&(cur_conf!=NULL))
						{
							strcpy(cur_conf->preproc,prepro);
						}
						else if(cur_gconf!=NULL)
						{
							strcpy(cur_gconf->preproc,prepro);
						}

					}
				}
			}
		
		}
		p=strtok(NULL,"<>");
	}

	free(file_buffer);


}


void project_t::WriteMakeFile(char *sol_dir,char *file,const char *conf_name,const char *make_dir,char *out_binary,unsigned int type)
{
	char	out_file[256];
	char	gbl_objects[256];
	int		i,j,k,list_file;
	FILE	*out;
	char	temp_tok[1024];
	char	command_line[1024*64];
	char ofile[512],oofile[512];
	char tab=9;
	int conf_index;

	list_file=0;

	strcpy(out_file,make_dir);
	strcat(out_file,"/");
	strcat(out_file,file);

	printf("Writing MakeFile %s \r\n",out_file);

	out	=fopen(out_file,"wb");

	fprintf(out,"SolutionDir=%s/\r\n",sol_dir);
	fprintf(out,"ConfigurationName=%s\r\n",conf_name);

	
	
	fprintf(out,"CC=gcc\r\n");

	memset(gbl_objects,0,255);
	conf_index=0;

	for(i=0;i<num_gbl_conf;i++)
	{
		char			proj_base_path[128];
		int				dirlen=dirname(gconfigs[i].source_project);
	
		strncpy(proj_base_path,gconfigs[i].source_project,dirlen);
		proj_base_path[dirlen]=0;

		if((conf_name==NULL)||(!strcmp(gconfigs[i].prefix,conf_name)))
		{
			char			*cpath;

			conf_index=i;

			fprintf(out,"TargetPath=%s/%s.so\r\n",sol_dir,out_binary);
			fprintf(out,"TargetName=%s\r\n",sol_dir,out_binary);
			fprintf(out,"ProjectName=%s.so\r\n",this->name);
			

			/*process objects */
			strcpy(gconfigs[i].obj_var,gconfigs[i].prefix);
			strcat(gconfigs[i].obj_var,"_obj");

			strcpy(gconfigs[i].target,"");
			for(j=0;j<num_files;j++)
			{
				for(k=0;k<files[j].num_conf;k++)
				{
					if( (!strcmp(files[j].confs[k].name,gconfigs[i].name))&&
						(!strcmp(files[j].source_proj,gconfigs[i].source_project)) )
					{
						strcat(gconfigs[i].target,files[j].confs[k].short_name);
						strcat(gconfigs[i].target," ");
					}
				}
			}


			/*process includes */
			strcpy(gconfigs[i].incs_var,gconfigs[i].prefix);
			strcat(gconfigs[i].incs_var,"_incs");

			strcpy(temp_tok,gconfigs[i].includes);
			cpath=strtok(temp_tok,";,");
			while(cpath!=NULL)
			{
				if(cpath[1]!=':')
				{
					if(!strncmp(cpath,"$(SolutionDir)",14))
					{
						strcpy(ofile,cpath);
					}
					else
					{
						get_real_path (ofile,proj_base_path,cpath);
					}
			
					if(is_path_inside(ofile,make_dir))
					{
						strcpy		  (oofile,"./");
						strcat		  (oofile,trim_left(ofile,make_dir));
					}
					else
					{
						strcpy		  (oofile,ofile);
					}
				}
				else
				{
					strcpy		  (oofile,cpath);
				}
				strcat(gconfigs[i].target_incs," -I");
				replace_abs_include(gconfigs[i].target_incs,oofile,path_list);
				cpath=strtok(NULL,";,");
			}

    		/*process dependences */
			strcpy(gconfigs[i].dep_var,gconfigs[i].prefix);
			strcat(gconfigs[i].dep_var,"_deps");

			for(int n=0;n<num_depends;n++)
			{
				
				strcat(gconfigs[i].target_dep,depend_list_name[n]);

				if( (!strcmp(depend_list_name[n],"freetype"))||
					(!strcmp(depend_list_name[n],"libpng")))
				{
					strcat(gconfigs[i].target_dep,".a ");
				}
				else
				{
					strcat(gconfigs[i].target_dep,".so ");
				}
			}


			/*process defines */
			strcpy(gconfigs[i].def_var,gconfigs[i].prefix);
			strcat(gconfigs[i].def_var,"_def");

			strcpy(gconfigs[i].target_def,"");
			strcpy(temp_tok,gconfigs[i].preproc);
			cpath=strtok(temp_tok,";,");
			while(cpath!=NULL)
			{
				if( (strcmp(cpath,"WIN32"))&&
					(strcmp(cpath,"_WINDOWS"))&&
					(strcmp(cpath,"NDEBUG")) &&
					(strcmp(cpath,"_LIB")) 
					)
				{

					strcat(gconfigs[i].target_def," -D ");
					strcat(gconfigs[i].target_def,cpath);
					strcat(gconfigs[i].target_def,"=1");
				}
				cpath=strtok(NULL,";,");
			}

			rep_bsl(gconfigs[i].target_incs);
			
			fprintf(out,"%s=%s\r\n",gconfigs[i].obj_var,gconfigs[i].target);
			fprintf(out,"%s=%s\r\n",gconfigs[i].incs_var,gconfigs[i].target_incs);
			fprintf(out,"%s=%s\r\n",gconfigs[i].def_var,gconfigs[i].target_def);
			fprintf(out,"%s=%s\r\n\r\n",gconfigs[i].dep_var,gconfigs[i].target_dep);

			strcat(gbl_objects," $(");
			strcat(gbl_objects,gconfigs[i].obj_var);
			strcat(gbl_objects,")");
		}
		//fprintf(out,"%s: %s\r\n\r",gconfigs[i].obj_var,gconfigs[i].target);
	}

	if( 
		(!strcmp(out_binary,"freetype"))||
		(!strcmp(out_binary,"libpng"))
	  )
	{
		fprintf(out,"%s.a: $(%s) %s \r\n",out_binary,gconfigs[conf_index].dep_var,gbl_objects);
	}
	else
	{
		fprintf(out,"%s.so: $(%s) %s \r\n",out_binary,gconfigs[conf_index].dep_var,gbl_objects);
	}
	fwrite (&tab,1,1,out);

	if(type==0)
	{
		int n_post_cmd;
		if(strlen(gconfigs[conf_index].target)>0)
		{
			//fprintf(out,"ar -r %s.a %s",out_binary,gbl_objects);

			if( 
				(!strcmp(out_binary,"freetype"))||
				(!strcmp(out_binary,"libpng"))
			  )

			{
				fprintf(out,"ar -r %s.a %s",out_binary,gbl_objects);
			}
			else
			{
				if(!strcmp(out_binary,"graphic_base"))
				{
					fprintf(out,"gcc " GCC32PARAM "--shared -nostdlib %s $(%s) freetype.a libpng.a -o %s.so ",gbl_objects,gconfigs[conf_index].dep_var,out_binary);
				}
				else
				{
					fprintf(out,"gcc " GCC32PARAM "--shared -nostdlib %s $(%s) -o %s.so ",gbl_objects,gconfigs[conf_index].dep_var,out_binary);
				}
			}
		}
		fprintf(out,"\r\n");


		n_post_cmd=0;
		while(n_post_cmd<gconfigs[conf_index].n_post_cmds)
		{
			if(strlen(gconfigs[conf_index].post_cmd[n_post_cmd].file_cmd)>0)
			{
				int n;
				fprintf(out,"\t%s ",gconfigs[conf_index].post_cmd[n_post_cmd].file_cmd);

				n=0;
				while(n<gconfigs[conf_index].post_cmd[n_post_cmd].n_args)
				{

					
					fprintf(out,"%s ",gconfigs[conf_index].post_cmd[n_post_cmd].args[n].str);
					n++;
				}
				fprintf(out,"\r\n");
			}
			n_post_cmd++;
		}
		
		
	}
#ifdef TARGET_PLAFORME_MACOS
	else if(type==2)
		fprintf(out,"g++ %s $(%s) $(LIBS) --shared -o %s.so \r\n",gbl_objects,gconfigs[conf_index].dep_var,out_binary);
	else if(type==3)
		fprintf(out,"g++ %s $(%s) $(LIBS) -dynamiclib -read_only_relocs warning -o %s.dylib \r\n",gbl_objects,gconfigs[conf_index].dep_var,out_binary);
	else
	{
		char frmwrks[]="-framework Carbon -framework CoreServices -framework AudioUnit -framework CoreAudio -framework AudioToolBox";
		
		fprintf(out,"g++ %s $(%s) $(LIBS) -bundle     -read_only_relocs warning %s -o %s \r\n",gbl_objects,gconfigs[conf_index].dep_var,frmwrks,out_binary);
	}
#endif
#ifdef TARGET_PLAFORME_LINUX
	else
	{
		fprintf(out,"g++ %s $(%s) $(LIBS) " GCC32PARAM "--shared -o %s/%s.so \r\n",gbl_objects,gconfigs[conf_index].dep_var,gconfigs[conf_index].output_dir,out_binary);
	}

#endif
	fprintf(out,"clean:\r\n");
	i=num_depends;
	while(i--)
	{
		fprintf(out,"\tmake -f %s clean \r\n",depend_list_mf[i]);
	}

	fprintf(out,"\trm -f %s\r\n",gbl_objects);
	if(type==2)
	{
		if( (!strcmp(out_binary,"freetype"))||
			(!strcmp(out_binary,"libpng"))
			)
		{
			fprintf(out,"\trm -f %s.a \r\n",out_binary);
		}
		else
		{
			fprintf(out,"\trm -f %s.so \r\n",out_binary);
		}
	}
	else if(type==3)
		fprintf(out,"\trm -f %s.dylib \r\n",out_binary);
	else if(type==0)
		fprintf(out,"\trm -f %s.so \r\n",out_binary);
	else
		fprintf(out,"\trm -f %s \r\n",out_binary);

	
	fprintf(out,"\r\n",out_binary);

	for(i=0;i<num_depends;i++)
	{
		if(
			(!strcmp(depend_list_name[i],"freetype"))||
			(!strcmp(depend_list_name[i],"libpng"))
			)

		{
			fprintf(out,"%s.a:\r\n",depend_list_name[i]);
		}
		else
		{
			fprintf(out,"%s.so:\r\n",depend_list_name[i]);
		}
		
		fprintf(out,"\tmake -f %s \r\n",depend_list_mf[i]);
		fprintf(out,"\r\n");
	}

	for(i=0;i<num_files;i++)
	{
		char			proj_base_path[128];
		int				dirlen=dirname(files[i].source_proj);
	
		strncpy(proj_base_path,files[i].source_proj,dirlen);
		proj_base_path[dirlen]=0;

		for(j=0;j<files[i].num_conf;j++)
		{

			char			*cpath;
			char			*define;
			global_config	*gc;

			gc=get_gconf(files[i].confs[j].name,files[i].source_proj);
			
			if((gc)&&(conf_name!=NULL))
			{
				if(strcmp(gc->prefix,conf_name))continue;
			}


			fprintf(out,"\r\n\r\n%s:%s \r\n",files[i].confs[j].short_name,files[i].make_path);

			fwrite(&tab,1,1,out);

			rep_bsl(files[i].make_path);
			
			if(files[i].ext_id==3)
			{
#ifdef TARGET_PLAFORME_LINUX
				strcpy(command_line,"nasm -f elf -DPREFIX ");
#endif
#ifdef TARGET_PLAFORME_MACOS
				strcpy(command_line,"nasm -f macho -DPREFIX ");
#endif
				strcat(command_line,files[i].make_path);
			}
			else
			{
				strcpy(command_line,"$(CC) -c " GCC32PARAM );
				strcat(command_line,files[i].make_path);
			}

			strcpy(temp_tok,files[i].confs[j].includes);
			cpath=strtok(temp_tok,";,");
			while(cpath!=NULL)
			{
				if(cpath[1]!=':')
				{
					get_real_path (ofile,proj_base_path,cpath);
			
					if(is_path_inside(ofile,make_dir))
					{
						strcpy		  (oofile,"./");
						strcat		  (oofile,trim_left(ofile,make_dir));
					}
					else
					{
						strcpy		  (oofile,ofile);
					}
				}
				else
				{
					strcpy		  (oofile,cpath);
				}

				rep_bsl(oofile);


				strcat(command_line," -I");
				strcat(command_line,oofile);
				cpath=strtok(NULL,";,");
			}
			strcat(command_line," $(");
			strcat(command_line,gc->incs_var);
			strcat(command_line,") ");

			strcat(command_line," $(");
			strcat(command_line,gc->def_var);
			strcat(command_line,") ");


			strcpy(temp_tok,files[i].confs[j].preproc);
			define=strtok(temp_tok,";,");
			while(define!=NULL)
			{
				if( (strcmp(define,"WIN32"))&&
					(strcmp(define,"_WINDOWS"))&&
					(strcmp(define,"NDEBUG")) &&
					(strcmp(define,"_LIB")) 
					)
				{

					strcat(command_line," -D ");
					strcat(command_line,define);
					strcat(command_line,"=1");
				}
				define=strtok(NULL,";,");
			}
			strcat(command_line," -o ");
			strcat(command_line,files[i].confs[j].short_name);
									
			fprintf(out,"%s\r\n",command_line);
		}
	}
	fclose(out);

}

char *get_next(char *buffer,int bufflen,char tok,int &count)
{
	count=0;

	while((buffer[count]!=0)&&(count<bufflen))
	{
		if(buffer[count]==tok)
		{
			return &buffer[count];
		}
		count++;
	}
	return NULL;
}
bool isvalid(char c)
{
	if(unsigned(c)+1>256)return 0;
	if(isalpha(c)||isdigit(c))return 1;
	if(c=='-')return 1;
	if(c=='_')return 1;
	return 0;


}

void get_first_word(const char *buffer,char *word,int word_len)
{
char read=0;

memset(word,0,word_len);

while(!isvalid(buffer[read]))
{
	if(buffer[read]==0)return;
	read++;
}
while(isvalid(buffer[read]))
{
	(*(word++))=buffer[read];
	read++;
}
*word=0;
}

void read_line(char *buffer,int &buff_read,int buff_len,
			   char *line,int line_len)
{
	char c;
	int rl=0;
	memset(line,0,line_len);
	while(  (buff_read<buff_len)&&
			((rl+1)<line_len))
	{
		c=buffer[buff_read];
		buff_read++;
		if(c==10)
		{
			line[rl]=0;
			return ;
		}
		else if(c!=13)
		{
			line[rl]=c;
		}
		rl++;
	}
	line[rl]=0;
}

project_t	*solution_t::GetProjectById(char *id)
{
	int i;
	for(i=0;i<num_project;i++)
	{
		if(!strcmp(projects[i].ID,id))
		{
			return &projects[i];
		}
	}
	return NULL;
}
void solution_t::ReadSln(char *file,char *MakeFilePath)
{
project_t   *proj_ptr;
char		*file_buffer;
int			 file_len;
int			 done,i,j,k;
int			 rl,file_read;
char		 c;
char		 line[1024];
char		 word[512];

FILE *s;
s=fopen(file,"rb");
if(!s)
{
	printf("Could Not Open Solution File : '%s' \r\n",file);
	return ;
}
fseek(s,0,SEEK_END);
file_len	=ftell(s);
rewind	(s);
file_buffer	=(char *)calloc(file_len+1,1);
fread	(file_buffer,1,file_len,s);
fclose	(s);


strncpy		 (basedir,file,dirname(file));
rep_bsl		 (basedir);


done		=0;
file_read	=0;
proj_ptr	=NULL;

while(!isvalid(file_buffer[file_read]))
{
	file_read++;
}


while(file_read<file_len)
{
	read_line		(file_buffer,file_read,file_len,line,1024);
	get_first_word	(line,word,512);
	if(!strcmp(word,"Project"))
	{
		int			 count,dirlen;
		char		 *proj,*pn;
		char         *trimed;
		char		 rel_path[255];
		char		tp[512];

		

		proj_ptr	= &projects[num_project];
		proj		= get_next(line,file_len-file_read,'=',count);
		proj++;
		
		pn			=get_next	(proj		 ,255 ,',',count);
		extract_quote(proj,count,proj_ptr->name);
		
		printf		 ("***** project name  : %s ******\r\n",proj_ptr->name);

		pn++;
		proj		=pn;
		pn			=get_next(proj		,255,',',count);

		
		
		extract_quote(proj,count,rel_path);
		rep_bsl		 (rel_path);

		printf		 ("project relative path : %s \r\n",rel_path);
		
		get_real_path(proj_ptr->file_path,basedir,rel_path);
		rep_bsl		 (proj_ptr->file_path);

		printf		 ("project real path : %s \r\n",proj_ptr->file_path);
		
		dirlen			=dirname(proj_ptr->file_path);
		strncpy	(tp		, proj_ptr->file_path,dirlen);
		tp[dirlen]=0;
		
		printf		 ("tp : %s , %s\r\n",tp,MakeFilePath);

		strcpy		(proj_ptr->make_file_path	, trim_left(tp,MakeFilePath));
		
		printf		 ("final path : %s \r\n",proj_ptr->make_file_path);


		strcat		(proj_ptr->make_file_path	, "/Makefile.");
		strcat		(proj_ptr->make_file_path	, proj_ptr->name);
		strcat		(proj_ptr->make_file_path	, MAKEFILE_SUFFIX);

		printf		 ("final file : %s \r\n",proj_ptr->make_file_path);

		extract_quote(pn+1,strlen(line),proj_ptr->ID);
		
		if(strlen(proj_ptr->name)>0)
			num_project++;
	}
	else 	if(!strcmp(word,"ProjectSection"))
	{

		while(1)
		{
			read_line		(file_buffer,file_read,file_len,line,1024);
			get_first_word	(line,word,512);

			if(!strcmp(word,"EndProjectSection"))
				break;

			if((proj_ptr)&&(strlen(word)>0))
			{
				strcpy(proj_ptr->depend_list_id[proj_ptr->num_depends],"{");
				strcat(proj_ptr->depend_list_id[proj_ptr->num_depends],word);
				strcat(proj_ptr->depend_list_id[proj_ptr->num_depends],"}");
				proj_ptr->num_depends++;
			}
		}
	}
	/*depend_list_id*/
}

for(i=0;i<num_project;i++)
{
	for(j=0;j<projects[i].num_depends;j++)
	{
		project_t *proj_ptr=GetProjectById(projects[i].depend_list_id[j]);
		if(proj_ptr)
		{
			if(strlen(proj_ptr->name)>0)
			{
				strcpy(projects[i].depend_list_name[j],proj_ptr->name);
				strcpy(projects[i].depend_list_mf[j]  ,proj_ptr->make_file_path);

				for(k=0;k<proj_ptr->num_depends;k++)
				{
					project_t *pproj_ptr=GetProjectById(proj_ptr->depend_list_id[k]);

					strcpy(proj_ptr->depend_list_name[k],pproj_ptr->name);
					strcpy(proj_ptr->depend_list_mf[k]  ,pproj_ptr->make_file_path);
				}
			}
		}
		else
		{
			printf("No Proj For Id '%s' \r\n",projects[i].depend_list_id[j]);
		}

	}
}

for(i=0;i<num_project;i++)
{
	printf("Read Project file : %s \r\n",projects[i].file_path);
	if(strlen(proj_ptr->name)>0)
	{
		projects[i].ReadVcFile	(projects[i].file_path,MakeFilePath,basedir);	
		projects[i].dump(stdout,i);	
	}
}
}
void	solution_t::WriteMakeFile(unsigned int proj_id,char *makefile_path,char *configuration)
{
	int i,j;
	char makefilename[255];
	project_t *dep_ptr,*ddep_ptr;
	if(proj_id>=num_project)return;

	strcpy		(makefilename	, "/Makefile.");
	strcat		(makefilename	, projects[proj_id].name);
	strcat		(makefilename	, MAKEFILE_SUFFIX);

	projects[proj_id].WriteMakeFile(basedir,makefilename,configuration,makefile_path,projects[proj_id].name,1);
	for(i=0;i<projects[proj_id].num_depends;i++)
	{
		dep_ptr=GetProjectById(projects[proj_id].depend_list_id[i]);
		
		dep_ptr->WriteMakeFile(basedir,projects[proj_id].depend_list_mf[i],configuration,makefile_path,projects[proj_id].depend_list_name[i],0);
		for(j=0;j<dep_ptr->num_depends;j++)
		{
			ddep_ptr=GetProjectById(dep_ptr->depend_list_id[j]);
			ddep_ptr->WriteMakeFile(basedir,dep_ptr->depend_list_mf[j],configuration,makefile_path,dep_ptr->depend_list_name[j],0);
			

		}
	}

}
//project_t	project;
solution_t	sol;
int main(int argc, char* argv[])
{
	char		source_file[255];
	char		makefile_path[255];
	char		makefile_pathf[255];
	char		config_name[255];

	if(argc<3)
	{
		printf("\r\nvcproj solution path [configuration] \r\n");
		return 0;
	}

	if(argc==4)
	{
		strcpy(config_name,argv[3]);
	}
	else
	{
		strcpy(config_name,"debug_macos_Win32");
	}

	strcpy		(makefile_path,argv[2]);
	rep_bsl		(makefile_path);	
	sol.ReadSln	(argv[1],makefile_path);

/*
	while( (argc--)>0)
	{
		strcpy				(source_file,argv[argc]);
		project.ReadVcFile	(source_file,makefile_path);
	}
*/
	printf("choose project numer :");
	char c[2];
	c[0]=getchar();
	c[1]=0;


	rep_bsl(makefile_path);
	sol.WriteMakeFile(atoi(c),makefile_path,config_name);

	//strcpy(makefile_pathf,makefile_path);
    //strcat(makefile_pathf,"/MakeFile.mac");
	//project.WriteMakeFile(makefile_pathf,"debug_macos_Win32",makefile_path);

	return 0;
}


