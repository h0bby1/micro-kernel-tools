#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>


#if !defined(MAXPATHLEN)
#define MAXPATHLEN  256
#endif


typedef void *mem_ptr;
typedef const void *const_mem_ptr;
typedef unsigned long mem_size;

mem_ptr memcpy_c(mem_ptr dst_ptr,const_mem_ptr src_ptr,mem_size size)
{
	const unsigned char *sptr	=src_ptr;
	unsigned char *dptr			=dst_ptr;
	unsigned int	n			=0;;

	while(n<size){dptr[n]=sptr[n];n++;}

	return dst_ptr;
	
}
mem_ptr memset_c(mem_ptr ptr,unsigned char v,mem_size size)
{
	unsigned char *cptr=ptr;
	while(size--){cptr[size]=v;  }
	return ptr;
}

void free_c(mem_ptr ptr)
{
	free(ptr);
}

mem_ptr calloc_c(mem_size sz,mem_size blk)
{
	return calloc(sz,blk);
}
char *dirname(const char *path)
{
    static char *bname;
    const char *endp;

    if (bname == NULL) {
        bname = (char *) malloc(MAXPATHLEN);
        if (bname == NULL)
            return (NULL);
    }

    /* Empty or NULL string gets treated as "." */
    if (path == NULL || *path == '\0') {
        strncpy(bname, ".", MAXPATHLEN);
        return (bname);
    }

    /* Strip trailing slashes */
    endp = path + strlen(path) - 1;
    while (endp > path && *endp == '/')
        endp--;

    /* Find the start of the dir */
    while (endp > path && *endp != '/')
        endp--;

    /* Either the dir is "/" or there are no slashes */
    if (endp == path) {
        strncpy(bname, *endp == '/' ? "/" : ".", MAXPATHLEN);
        return (bname);
    }

    do {
        endp--;
    } while (endp > path && *endp == '/');

    if (endp - path + 2 > MAXPATHLEN) {
        return (NULL);
    }
    strcpy(bname, path);
    bname[(endp - path) + 1] = 0;

    return (bname);
}

char *basename (char* path)
{
	char *ptr = strrchr (path, '/');
	return ptr ? ptr + 1 : (char*)path;
}

void rep_string(char *string)
{
	unsigned int		n;
	n=strlen(string);
	while(n--){ if(string[n]==0)return; if(string[n]=='\\')string[n]='/'; }
}



int main(int argc,char **argv)
{
	char path[1024];
	char target_path[1024];
	int				n_args;
	int				compress_level;
	FILE			*orig,*comp;
	unsigned char	*file_data,*comp_data;
	unsigned int	filesz,compsz;

	compress_level	=	0;
	n_args			=	1;

	while(n_args<argc)
	{
		if(argv[n_args][0]!='-')break;
		
		if(argv[n_args][1]=='z')
		{
			compress_level	=	argv[n_args][2]-48;
		}
		n_args++;
	}

	if(argc>1)
	{
		strcpy(path,argv[n_args]);
	}
	else
	{
		printf("no file specified\n");
		return 0;
	}




		
	orig		=fopen(path,"rb");
	fseek			(orig,0,SEEK_END);
	filesz=ftell	(orig);
	fseek			(orig,0,SEEK_SET);
	file_data	=	calloc(filesz,1);
	fread			(file_data,filesz,1,orig);
	fclose			(orig);
	
	comp_data		=	calloc(filesz,1);
	compsz			=	filesz;
	compress2		(comp_data, &compsz, file_data, filesz,compress_level);

	//strcat						(target_file,".z");
	comp					=fopen(path,"wb");
	fwrite					("ZCMP" ,4,1,comp);
	fwrite					(&filesz,4,1,comp);
	fwrite					(&compsz,4,1,comp);
	fwrite					(comp_data,compsz,1,comp);
	fclose					(comp);


}